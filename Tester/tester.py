import tensorflow as tf
import numpy as np
from sklearn.metrics import accuracy_score


class Tester:
    def __init__(self, fewshotmodel, val_dataset):
        self.model = fewshotmodel
        self.val_dataset = val_dataset

        # fewshotmodel is an object of class FewShotModel(*args)
        # val_dataset is an object of class val_dataset() having method sample_episode.
        # sample_episode returns X_train, X_test, y_train, y_test

    def test(self, samples=100):
        self.accuracy = []
        for i in range(samples):
            X_train, X_test, y_train, y_test = self.val_dataset.sample_episode(
                *args)
            self.prediction = self.model.predict(X_test)
            self.accuracy.append(accuracy_score(y_test, self.prediction))

        sz = 100

        # calculate 95% CI using bootstrap

        bootstrap = np.random.choice(self.accuracy,
                                     size=(sz, self.accuracy.shape[0]),
                                     replace=True)
        bootstrap = np.sort(np.mean(bootstrap, 1))
        quant_left = int(2.5 * sz // 100)
        left_bound = bootstrap[quant_left]
        right_nound = bootstrap[-quant_left]

        # return mean accuracy, std accuracy and 95% CI to accuracy

        return 'metric: accuracy,\nmean: {:.2f},\nstd: {:.2f},\n95% conf interval: [{:.2f} ,{:.2f}]'.format(
            np.mean(self.accuracy), np.std(self.accuracy), left_bound,
            right_bound)