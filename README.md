Модуль Tester лежит в папке Tester, файл tester.py


Логика следующая:

1) Класс Tester на вход принимает объект класса [FewShotModel](https://github.com/MokriyYuriy/FewShotLearning/blob/master/algorithms/few_shot_model.py), который умеет делать predict() по заданным X_train, y_train

2) Класс Tester также принимает на вход объект класса val_dataset, который семплирует эпизоды, возвращая
X_train, X_test, y_train, y_test

3) Tester содержит метод test(), который имеет параметр samples - количество раз, которое мы сэмплируем из val_dataset.
По сэмплам вычисляется среднее accuracy, std accuracy, а также с помощью бутстрепа оценивается 95% доверительный интервал
